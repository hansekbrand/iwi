# README #

## What is this repository for? ##

With this R package you can automatically derive the international
wealth index for DHS-data.

1.0 Release date 2025-02-26

## Introduction

I'm in a hurry, what are the instructions for a minimal data set? 

Include "wealth" as an element in `variable.packages` in the call to `globallivingconditions::download.and.harmonise()`

    install.packages("devtools")
    library(devtools)
	install_bitbucket(repo = "hansekbrand/iwi") ## install iwi first, since DHSharmonisation depends on it.
	install_bitbucket(repo = "hansekbrand/DHSharmonisation")
	library(globallivingconditions)
	my.dt <- download.and.harmonise(
	  dhs.user="", <-- your username at DHS goes here
	  dhs.password="", <-- your password at DHS goes here
	  vars.to.keep=c("source", "country.code.ISO.3166.alpha.3", "version",
                  "year.of.interview", "ClusterID", "HHID", "PersonID.unique",
				  "iwi", "lon", "lat"),
       variable.packages = c("wealth"),
       countries=c("Angola", "Benin", "Burkina Faso", "Burundi", "Cameroon", "Ghana",
            "Chad", "Congo", "Congo Democratic Republic", "Cote d'Ivoire", 
            "Eswatini", "Ethiopia", "Gabon", "Gambia", "Ghana", "Guinea", 
            "Kenya", "Liberia", "Madagascar", "Malawi", "Mali", "Mozambique",
            "Namibia", "Niger", "Nigeria", "Rwanda", "Senegal", "Sierra Leone",
            "Tanzania", "Togo", "Uganda", "Zambia"),
    file.types.to.download = c("PR", "GE"),
    log.filename = "living-conditions.log")


`my.dt` will now have a minimal data set with the International Wealth index. Note that if you have access to many countries `download.and.harmonise()` will take many hours.

<!-- If you want to get the raw data used for the IWI derivation, e.g. to -->
<!-- get the water variable HV205, include "wealth.205", and more generally use: -->

<!-- 		vars.to.keep=c("source", "country.code.ISO.3166.alpha.3", "version", -->
<!--         "year.of.interview", "ClusterID", "HHID", "PersonID.unique", -->
<!-- 		"iwi", "lon", "lat", "wealth.HV201", "wealth.HV205", -->
<!-- 		"wealth.HV206", "wealth.HV207", "wealth.HV208", "wealth.HV209", -->
<!-- 		"wealth.HV210", "wealth.HV212", "wealth.HV213", "wealth.HV216", -->
<!-- 		"wealth.HV221", "wealth.HV243A" -->

<!-- If you want are a developer and to derive _weights_ for calculation of IWI, use -->

<!-- 	    iwi.weights <- iwi::derive.weights(my.dt) -->
		
<!-- but this is still under construction. -->

## Who do I talk to? ##

> Hans Ekbrand <hans.ekbrand@socavgu.se>
> University of Gothenburg, department of sociology and work science
> Sweden
